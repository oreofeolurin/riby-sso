import * as sequelize from "sequelize";

export class PostgresDBHandle{

    private connected: boolean = false;
    public handle: sequelize.Sequelize;

    constructor() {

        let DBHandle = new sequelize(process.env['POSTGRES_DB_URI']);

        DBHandle
            .authenticate().then(() =>{
                console.info("Connected to PostgresDB service");
                this.connected = true;
                //connector.ready();
            })
            .catch(err => {
                console.error("Error connecting to PostgresDB service %s", err);
                this.connected = false;
                //DBHandle.end();
                //connector.lost();
            });

        this.handle = DBHandle;
    }

    public getHandle(){
        return this.handle;
    }

    public close(){
        this.handle.close();
    }

}