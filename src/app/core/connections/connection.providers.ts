import {RabbitMQClient} from "./rabbitmq-client";
import {MongodbHandle} from "./mongodb-handle";

export const CONNECTION_PROVIDERS = [
    {
        provide: RabbitMQClient,
        useFactory: () => {
            return new RabbitMQClient(process.env['RABBIT_MQ_URL']);
        }
    }
];