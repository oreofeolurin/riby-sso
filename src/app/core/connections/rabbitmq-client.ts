import * as amqp from 'amqplib';
import { ClientProxy } from '@nestjs/microservices';
import {QueueableData} from "../interfaces/queueable.data";

type MessageObject = {pattern: object, data: QueueableData}

export class RabbitMQClient extends ClientProxy {
    private server: amqp.Connection;
    private channel: amqp.Channel;

    constructor(
        private readonly host: string) {
        super();
    }

    protected async sendSingleMessage(messageObj : MessageObject, callback: (err, result, disposed?: boolean) => void) {
        if(!this.server || !this.channel) {
            this.server = await amqp.connect(this.host);
            this.channel = await this.server.createChannel();
        }

        let queue  = messageObj.data.queueName;

        this.channel.assertQueue(queue, {durable: true});
        this.channel.sendToQueue(queue, Buffer.from(JSON.stringify(messageObj)), {persistent: true});
    }

}