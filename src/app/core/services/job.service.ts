/**
 * Created by EdgeTech on 12/11/2016.
 */
import {Component} from "@nestjs/common";
import {Job} from "../jobs/job";
import {RabbitMQClient} from "../connections/rabbitmq-client";
import {ClientProxy} from "@nestjs/microservices";
import {QueueTasks} from "../helpers/enums";



@Component()
export class JobService {
    private client: ClientProxy;

    constructor() {
        this.client = new RabbitMQClient(process.env['RABBIT_MQ_URL']);
    }

    public addJobToQueue(job: Job, task: QueueTasks){
        this.client.send<number>(task, job).subscribe();
    }

}