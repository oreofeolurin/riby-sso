import * as mongoose from "mongoose";
import {Model} from "../interfaces/model";
import {MongodbException} from "../exceptions/mongodb.exception";
import {Utils} from "../helpers/utils";
import {HttpStatus} from "@nestjs/common";
import {AppV2Exception} from "../exceptions/app-v2.exception";
import * as Sequelize from "sequelize";
import {ModelAttributes, ModelInstance} from "./model";
import {DatabaseException} from "../exceptions/database.exception";


/**
 * An abstract class for helping with common repository functions
 * for interacting with the model of the concrete class.
 *
 * @author  Oreofe Olurin
 * @version 0.0.1
 * @since   2017-10-21
 */

export abstract class Repository {
    protected model: Sequelize.Model<ModelInstance<ModelAttributes>, ModelAttributes>;

    private UNKNOWN_DOCUMENT_EXCEPTION: AppV2Exception;

    constructor(model: Model) {
        this.model = model.getModel();

        this.UNKNOWN_DOCUMENT_EXCEPTION =
            new AppV2Exception("Unknown " + Utils.toTitleCase(this.model.getTableName()), HttpStatus.BAD_REQUEST);
    }

    public async create(document) {

        try {
            await this.model.sync({force: true});
            return await this.model.create(document);
        } catch (err) {
            throw new DatabaseException(err);
        }
    }


    public findOneById(id: string) {
        return this.findOne({_id : id});
    }

    public updateById(id: string, updateObject: ModelAttributes) {
        return this.update({id: id}, updateObject);
    }


    public deleteById(id: string) {
        return this.remove({_id : id});
    }


    /**
     * Gets all document in the database
     *
     * @param {Object} conditions
     * @return {Promise<any>}
     */
    public find(conditions: object){

        return new Promise((resolve, reject) => {

            this.model
                .find(conditions)
                .lean()
                .exec(function (err, docs: Array<mongoose.Document>) {

                    if (err) return reject(new Error(err));

                    return resolve(docs);

                });

        })

    }

    /**
     * Gets one document in the database
     * @param {Object} conditions
     * @return {Promise<any>}
     */
    public findOne(conditions: object){

        const self = this;

        return new Promise((resolve, reject) => {

            this.model
                .findOne(conditions)
                .lean()
                .exec(function (err, doc: mongoose.Document) {

                    if (err) return reject(new Error(err));

                    if(!doc)
                        return reject(self.UNKNOWN_DOCUMENT_EXCEPTION);

                    return resolve(doc);

                });

        })

    }


    /**
     * Updates an existing document
     *
     * @param conditions
     * @param update
     * @returns {Promise<string>}
     */
    protected async update(conditions: Sequelize.AnyWhereOptions, update: ModelAttributes){

        const self = this;

        try {
            await this.model.sync();
            let updated = await this.model.update(update, {where: conditions});

            console.log('updated[0]');
            console.log(updated[0])

            if (updated[0] < 1)
                throw self.UNKNOWN_DOCUMENT_EXCEPTION;

            return true;

        } catch (err) {
            throw new DatabaseException(err);
        }
    }


    /**
     * Removes an existing document
     *
     * @param {Object} conditions
     * @return {Promise<any>}
     */
    public remove(conditions: object){

        return new Promise((resolve, reject) => {

            this.model.remove(conditions)
                .exec((err)=> {
                console.log("error is");
                console.log(err);
                    if (err) return reject(new MongodbException(err));

                    return resolve(true);
                });
        })
    }









}