import * as mongoose from "mongoose";
import {Model} from "../interfaces/model";
import {MongodbException} from "../exceptions/mongodb.exception";
import {AppException} from "../exceptions/app.exception";
import {Utils} from "../helpers/utils";
import {HttpStatus} from "@nestjs/common";
import {AppV2Exception} from "../exceptions/app-v2.exception";


/**
 * An abstract class for helping with common repository functions
 * for interacting with the model of the concrete class.
 *
 * @author  Oreofe Olurin
 * @version 0.0.1
 * @since   2017-10-21
 */

export abstract class Repository {
    protected model: mongoose.Model<mongoose.Document>;
    private UNKNOWN_DOCUMENT_EXCEPTION: AppV2Exception;

    constructor(model: Model) {
        this.model = model.getModel();

        this.UNKNOWN_DOCUMENT_EXCEPTION =
            new AppV2Exception("Unknown " + Utils.toTitleCase(this.model.collection.collectionName), HttpStatus.BAD_REQUEST);
    }

    public create(document) {

        //Lets create a skeleton user with provided form params
        let doc = new this.model(document);

        return this.save(doc);
    }


    public findOneById(id: string) {
        return this.findOne({_id : id});
    }

    public updateById(id: string, updateObject: object) {
        return this.update({_id: id}, { $set : updateObject});
    }


    public deleteById(id: string) {
        return this.remove({_id : id});
    }


    /**
     * Saves a document in DB
     *
     * @param {"mongoose".Document} document
     * @return {Promise<Object>}
     */
    public save(document: mongoose.Document): Promise<mongoose.Document>{

        return new Promise(function(resolve,reject) {

            document.save((err, doc) => {
                if (err) return reject(new MongodbException(err));

                return resolve(doc);
            });
        })

    }


    /**
     * Gets all document in the database
     *
     * @param {Object} conditions
     * @return {Promise<any>}
     */
    public find(conditions: object){

        return new Promise((resolve, reject) => {

            this.model
                .find(conditions)
                .lean()
                .exec(function (err, docs: Array<mongoose.Document>) {

                    if (err) return reject(new Error(err));

                    return resolve(docs);

                });

        })

    }

    /**
     * Gets one document in the database
     * @param {Object} conditions
     * @return {Promise<any>}
     */
    public findOne(conditions: object){

        const self = this;

        return new Promise((resolve, reject) => {

            this.model
                .findOne(conditions)
                .lean()
                .exec(function (err, doc: mongoose.Document) {

                    if (err) return reject(new Error(err));

                    if(!doc)
                        return reject(self.UNKNOWN_DOCUMENT_EXCEPTION);

                    return resolve(doc);

                });

        })

    }


    /**
     * Updates an existing document
     *
     * @param conditions
     * @param update
     * @returns {Promise<string>}
     */
    protected update(conditions: object, update: object){

        const self = this;

        return new Promise((resolve, reject)  => {
            this.model.update(conditions, update).exec((err, raw)=> {

                if (err)
                    return reject(new MongodbException(err));


                if (raw.nModified < 1)
                    return reject(self.UNKNOWN_DOCUMENT_EXCEPTION);

                return resolve(true);
            });
        })
    }


    /**
     * Removes an existing document
     *
     * @param {Object} conditions
     * @return {Promise<any>}
     */
    public remove(conditions: object){

        return new Promise((resolve, reject) => {

            this.model.remove(conditions)
                .exec((err)=> {
                    if (err) return reject(new MongodbException(err));

                    return resolve(true);
                });
        })
    }



}