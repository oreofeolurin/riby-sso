
import * as Sequelize from "sequelize";

export interface ModelInstance<T> extends ModelAttributes, Sequelize.Instance<T> {

}

export interface ModelAttributes {
    id: string
}