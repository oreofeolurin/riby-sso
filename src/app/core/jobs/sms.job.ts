import {Job} from "./job";
import {WorkerQueue} from "../helpers/enums";



export class SMSJob extends Job{

    private from : string;
    private to : string;
    private body : string;

    constructor(){
        super(WorkerQueue.PROCESS_WORK);
    }

    public setFrom(value: string) {
        this.from = value;
        return this;
    }

    public setTo(value: string) {
        this.to = value;
        return this;
    }


    public setBody(value: string) {
        this.body = value;
        return this;
    }



}