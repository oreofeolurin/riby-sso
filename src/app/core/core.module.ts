import {Module} from "@nestjs/common";
import {EventPublisher} from "./events/event.publisher";
import {JobService} from "./services/job.service";
import {MongodbHandle} from "./connections/mongodb-handle";
import {ShorterService} from "./services/shorter.service";
import {PostgresDBHandle} from "./connections/postgresdb-handle";

@Module({
    components: [MongodbHandle, PostgresDBHandle,  EventPublisher, JobService, ShorterService],
    exports: [ MongodbHandle, PostgresDBHandle,  EventPublisher, JobService, ShorterService]
})
export class CoreModule{}