/**
 * The base Exception class
 * ```
 */
import {HttpException} from "@nestjs/core";

export abstract class Exception extends HttpException {
    public error;

    constructor(err: any, status: number, private message: string) {
        super(typeof err === "string" ? err : message, status);
        this.setError(err);
    }


    public setError(err){
        this.error = err;
        return this;
    }

    public getError(): any {
        return this.error;
    }

    public getMessage(): string {
        return this.handleError();
    }


    public handleError(): string{
        return this.message;
    }
}
