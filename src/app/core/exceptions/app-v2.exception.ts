import {HttpStatus} from "@nestjs/common";
import {Exception} from "./exception";

export class AppV2Exception extends Exception{

    constructor(err: any, status: number = HttpStatus.INTERNAL_SERVER_ERROR,  message = "App Exception") {
        super(err, status, message);
    }


    public static get UNKNOWN_USER(){
        return new this("Unknown User", HttpStatus.BAD_REQUEST);
    }

    public static get UNKNOWN_COOPERATIVE(){
        return new this("Unknown Cooperative", HttpStatus.BAD_REQUEST);
    }

    public static get UNKNOWN_DOCUMENT(){
        return new this("Unknown Document", HttpStatus.BAD_REQUEST);
    }

    public static get UNKNOWN_MEMBER(){
        return new this("Unknown Member", HttpStatus.BAD_REQUEST);
    }

    public static get UNKNOWN_ADMIN(){
        return new this("Unknown Admin", HttpStatus.BAD_REQUEST);
    }

    public static get INVALID_OTP(){
        return new this("Invalid OTP", HttpStatus.BAD_REQUEST);
    }

    public static get INVALID_PHONE_FORMAT(){
        return new this("Invalid phone format", HttpStatus.BAD_REQUEST);
    }

    public static get UNKNOWN_ROLE(){
        return new this("Unknown Role", HttpStatus.BAD_REQUEST);
    }

    public static get EXPIRED(){
        return new this("Resource has Expired", HttpStatus.UNAUTHORIZED);
    }


    public static get INVALID_PASSWORD(){
        return new this("Invalid Password", HttpStatus.UNAUTHORIZED);
    }

    public static get ACCOUNT_UNCONFIRMED(){
        return new this("Account Unconfirmed", HttpStatus.UNAUTHORIZED);
    }

    public static get INVALID_TOKEN(){
        return new this("Invalid Token", HttpStatus.UNAUTHORIZED);
    }


    public static get ACCOUNT_NOT_ACTIVE(){
        return new this("Account not active", HttpStatus.UNAUTHORIZED);
    }


}