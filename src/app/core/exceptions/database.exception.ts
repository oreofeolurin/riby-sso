import {Exception} from "./exception";
import {HttpStatus} from "@nestjs/common";

export class DatabaseException extends Exception {


    constructor(err: any) {
        super(err, HttpStatus.INTERNAL_SERVER_ERROR, 'Database error');
    }

    public handleError() {

        switch (this.getError().name) {
            case 'SequelizeValidationError':
            case 'SequelizeUniqueConstraintError':
                return this.getValidationMessage();
        }
    }

    public getValidationMessage() {
        return this.getError().errors[0].message;
    }

}