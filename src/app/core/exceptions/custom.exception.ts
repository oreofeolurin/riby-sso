import {HttpException} from "@nestjs/core";



export class CustomException extends HttpException {

    constructor(protected err: any, name: string, status: number) {
        super(name, status);
    }

    public getMessage(): string {
        return this.handleError();
    }

    public getError(): Object{

      //  if(process.env.NODE_ENV == "development")
            return this.err;
    }

    public handleError(): string{
        return undefined;
    }

}