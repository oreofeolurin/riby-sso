import {HttpException} from "@nestjs/core";
import {Catch, ExceptionFilter} from "@nestjs/common";


@Catch(HttpException)
export class HttpExceptionFilter implements ExceptionFilter {

    public catch(exception: HttpException, response) {
        const status = exception.getStatus();

        response.status(status).json({
            status: status,
            message: exception.getResponse(),
        });
    }
}