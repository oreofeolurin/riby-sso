import {Catch, ExceptionFilter} from "@nestjs/common";
import {CustomException} from "./custom.exception";
import {Exception} from "./exception";


@Catch(CustomException, Exception)
export class CustomExceptionFilter implements ExceptionFilter {

    public catch(exception: Exception , response) {
        const status = exception.getStatus();

        response.status(status).json({
            code: status,
            message: exception.getMessage(),
            error: exception.getError()
        });
    }
}