
import {HttpStatus} from "@nestjs/common";
import {Utils} from "../helpers/utils";
import {CustomException} from "./custom.exception";

export class MongodbException extends CustomException {

    constructor(err: any) {
        super(err, 'Database error', HttpStatus.INTERNAL_SERVER_ERROR);
    }

    public handleError(){
        let message : string;

       switch (this.err.code) {
           case 11000:
           case 11001:
               message = this.getDuplicateMessage();
               break;

           default:
               message = this.err.errmsg || "Service Unavailable";
       }

       return message;
    }

    private getDuplicateMessage(){
        let index;
        let indexMatch = this.err.errmsg.match("index:\\W+(\\w+)");

        if(indexMatch) {
            index = indexMatch[1];
            index = index.split("_")[0] || index;
            index = Utils.toTitleCase(index);
            return `${index} Already Exists`;
        }

        return 'Index Already Exists';
    }

}