import * as crypto from "crypto";
import * as _ from "lodash";
import * as moment from "moment-timezone";
import {phoneUtil, e164, formatNumber} from "libphonenumber";
import {AppException} from "../exceptions/app.exception";


export class Utils {

    public static generateRandomNumber(length) : number {

        // Generate a random {length} digit code
        let digit = Math.floor(Math.random()*Math.pow(10,length));

        if(digit.toString().length == length)
            return digit;

        return Utils.generateRandomNumber(length);
    }


    public static generateAppID() : string {

        let str =  Math.random().toString(36).substr(2, 16);
        let array = str.split(/(?=(?:....)*$)/);

        return array.join("-").toUpperCase();
    }

    public static generateRandomID(length) : string {

        let id =  crypto.randomBytes(length/2).toString('hex');

        if(id.length == length)
            return id;

        return Utils.generateRandomID(length);
    }


    public static noCache(req, res, next) {
        res.header('Cache-Control', 'private, no-cache, no-store, must-revalidate');
        res.header('Expires', '-1');
        res.header('Pragma', 'no-cache');
        next();
    }




    public static parseBuffer(buffer): Buffer{
        return JSON.parse(buffer, (key, value) => {
            return value && value.type === 'Buffer'
                ? new Buffer(value.data)
                : value;
        });

    }

    public static hashString(data:  string, algo?: string, digest?: crypto.HexBase64Latin1Encoding) {
        if(typeof algo === "undefined") algo = "md5";
        if(typeof digest === "undefined") digest = "hex";

        return crypto.createHash(algo).update(data).digest(digest);
    }


    public static getBooleanValue(value: any){

        const trueValues = ["true",true,"1",1];

        if(typeof value === "string")
            value = value.toLowerCase();


        return trueValues.some( element => element === value);
    }


    public static toSlug(value: string) {
        //lets clean
        //value = value.replace(/[^\w]/gi, '');

        //put dashes
        //return value.replace(/([A-Z])/g, "-$1").toLowerCase();

        return _.kebabCase(value)

    }

    public static toTitleCase(str) {
        //return str.toLowerCase().replace(/\b(\w)/g, s => s.toUpperCase());

        return _.startCase(str)
    }

    public static toCamelCase(str){

        //return str.replace(/-([a-z])/g, function (g) { return g[1].toUpperCase(); });

        return _.camelCase(str);
    }




    public static daysBetween(date1,date2){

        //Get 1 day in seconds
        let one_day=60*60*24;

        // Get difference in seconds
        let differenceInSeconds = Utils.secondsBetween(date1,date2);

        // Convert back to days and return
        return Math.round(differenceInSeconds/one_day);

    }


    public static secondsBetween(date1,date2){
        let moment1 = moment(date1);
        let moment2 = moment(date2);

        return Math.round(moment1.diff(moment2)/1000);
    }


    public static getPhoneE164(number: string,  countryCode: string = "+1"): Promise<string>{
        let  phoneNumber;

        return new Promise((resolve,reject)=>{
            try {
                if(typeof countryCode !== "undefined"){
                    let regionCode = phoneUtil.getRegionCodeForCountryCode(countryCode);
                    phoneNumber = phoneUtil.parse(number, regionCode);
                }
                else{
                    phoneNumber = phoneUtil.parse(number);
                }

                if(!phoneUtil.isValidNumber(phoneNumber))
                    return reject(AppException.INVALID_PHONE_FORMAT);


                return resolve(phoneUtil.format(phoneNumber, 0));

            }catch(err){
                return reject(AppException.INVALID_PHONE_FORMAT.setError(err.toString()));
            }
        })

    }


    public static enumToArray(_enum){
        let enumsArray = [];
        for(let prop in _enum){
            enumsArray.push(_enum[prop])
        }

        return enumsArray;
    }


    /**
     * Removes all Nil values from object - undefined, null
     *
     * @param {Object} obj
     * @return {object}
     */
    public static removeNilValues(obj: object){
        return _.omitBy(obj, _.isNil);
    }


    /**
     * Removes all Nil values from object - undefined, null
     *
     * @param {Object} obj
     * @param {String} spaceSeparatedKeys
     * @return {object}
     */

    public static selectKeys(obj: object, spaceSeparatedKeys: string){
        let keyArray = spaceSeparatedKeys.split(" ");

        let deep

        return _.pickBy(obj, function (value, key) {
            return _.includes(keyArray, key);
        });
    }




}