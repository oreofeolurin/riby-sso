export enum EmailTemplates {
    USER_WELCOME = "user-welcome",
    MEMBER_WELCOME = "member-welcome",
    ADMIN_WELCOME = "admin-welcome",
    RESET_PASSWORD = "reset-password"
}


export enum WorkerQueue  {
    PROCESS_WORK = "riby.jobs.process.work",
}

export enum QueueTasks  {
    SEND_SMS = 'task.send.sms',
    SEND_EMAIL = 'task.send.email'
}

export enum Authentications {
    RJT = "RJT",
    RSK = "RSK"
}

export enum CooperativeTypes {
    clusters = "clusters"
}

export enum AccountStatus {
    PENDING = "PENDING",
    ACTIVE = "ACTIVE",
    SUSPENDED = "SUSPENDED",
    TERMINATED = "TERMINATED"
}
