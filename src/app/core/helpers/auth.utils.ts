import {AppException} from "../exceptions/app.exception";
import * as jwt from 'jsonwebtoken';

export class AuthUtils{

    public static decodeJwtToken(token : string): Promise<{user: Object}>{

        return new Promise((resolve, reject) => {

            jwt.verify(token,process.env.JWT_SECRET, function (err, token) {
                if(err) return reject( AppException.INVALID_TOKEN);

                return resolve(token);
            })

        })

    }
}