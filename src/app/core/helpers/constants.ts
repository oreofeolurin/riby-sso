export const WorkerQueues = {
    PROCESS_EMAIL : "riby.jobs.process.email",
    PROCESS_SMS : "riby.jobs.process.sms"
};

export const ACCOUNT_STATUS_TYPES = {
    PENDING: "PENDING",
    ACTIVE: "ACTIVE",
    SUSPENDED : "SUSPENDED",
    TERMINATED: "TERMINATED"
};

export const ADMIN_ROLE_TYPES = {
    SUPER_ADMIN: "SUPER_ADMIN",
    ADMIN: "ADMIN"
};

export const EmailTemplates = {
    USER_WELCOME: "user-welcome",
    MEMBER_WELCOME: "member-welcome",
    RESET_PASSWORD: "reset-password"
};


export const SMSTemplates = {
    USER_WELCOME: (verifyLink) => `Please complete your registration with this code, ${verifyLink}`,
    MEMBER_WELCOME: (fistName, cooperativeName, verifyLink) => `Dear ${fistName}, a cooperative account was created on ${cooperativeName} for you. Please visit ${verifyLink} to set your password.`,
    ADMIN_WELCOME: (fistName, cooperativeName, verifyLink) => `Dear ${fistName}, a cooperative account was created on ${cooperativeName} for you. Please visit ${verifyLink} to set your password.`,
    RESET_PASSWORD : (resetLink) => `Please reset your password by visiting this link, ${resetLink}`
};

export const AppMessageSender = "Riby RFC";