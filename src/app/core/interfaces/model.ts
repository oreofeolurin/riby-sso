/**
 * Created by EdgeTech on 12/15/2016.
 */
import * as mongoose from 'mongoose';

/**
 * Model interface, All models must implement this model
 *
 * @author  Oreofe Olurin
 * @version 0.0.1
 * @since   2017-10-21
 */

export interface Model{

    /**
     * Gets the mongoose Document
     *
     */
    getModel();

}