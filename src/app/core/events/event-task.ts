


import {IEvent} from "./event.interface";

export class EventTask implements IEvent{

    readonly consumer: string;
    readonly name: string;
    readonly data: Object;

    constructor(consumer: string, name: string, data: Object = null){
        this.consumer = consumer;
        this.name = name;
        this.data = data;

    }
}

export enum EventTasks {
    USER_CREATED = "USER_CREATED",
    MEMBER_CREATED = "MEMBER_CREATED",
    ADMIN_CREATED = "ADMIN_CREATED",

    EMAIL_CONFIRMATION_REQUEST = "EMAIL_CONFIRMATION_REQUEST",
    PHONE_CONFIRMATION_REQUEST = "PHONE_CONFIRMATION_REQUEST",

    EMAIL_RESET_PASSWORD_REQUEST = "EMAIL_RESET_PASSWORD_REQUEST",
    PHONE_RESET_PASSWORD_REQUEST = "PHONE_RESET_PASSWORD_REQUEST",


    ROLES_AND_FUNCTIONS_CHANGED = "ROLES_AND_FUNCTIONS_CHANGED",
};