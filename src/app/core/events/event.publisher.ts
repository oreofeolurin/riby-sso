import {IEvent} from "./event.interface";
import {Component} from "@nestjs/common";
import { Subject} from "rxjs";

@Component()
export class EventPublisher{

    // Observable subjects;
    private eventSubject = new Subject<IEvent>();

    //Observable streams
    public eventStreams = this.eventSubject.asObservable();

    public publish(event: IEvent){
        this.eventSubject.next(event);
    }
}