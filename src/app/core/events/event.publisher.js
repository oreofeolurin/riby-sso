"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var common_1 = require("@nestjs/common");
var rxjs_1 = require("rxjs");
var EventPublisher = (function () {
    function EventPublisher() {
        // Observable subjects;
        this.eventSubject = new rxjs_1.Subject();
        //Observable streams
        this.eventStreams = this.eventSubject.asObservable();
    }
    EventPublisher.prototype.publish = function (event) {
        this.eventSubject.next(event);
    };
    EventPublisher = __decorate([
        common_1.Component()
    ], EventPublisher);
    return EventPublisher;
}());
exports.EventPublisher = EventPublisher;
