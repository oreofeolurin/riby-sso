export interface IEvent{
    consumer: string,
    name: string,
    data: Object
}