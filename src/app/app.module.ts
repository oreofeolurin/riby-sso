import {Module} from '@nestjs/common';
import {AccountModule} from "./account/account.module";
import {ApiModule} from "./api/api.module";

@Module({
  modules : [ApiModule, AccountModule],
})
export class ApplicationModule {}
