import {Component} from '@nestjs/common';
import {EventPublisher} from "../core/events/event.publisher";
import {AccountConsumer} from "./account.consumer";
import {ConfirmDto, CredentialDto, PhoneEmailDto, ResetAccountDto} from "./account.dto";
import * as jwt from 'jsonwebtoken';
import {EventTask, EventTasks} from "../core/events/event-task";
import {Utils} from "../core/helpers/utils";
import {UserRepository} from "../api/user/user.repository";
import {RoleService} from "../api/role/role.service";
import {User} from "../api/user/user.interface";

@Component()
export class AccountService {

    constructor(private readonly repository: UserRepository,
               private readonly eventPublisher: EventPublisher,
                private readonly roleService: RoleService){}

    /**
     * Confirms the account by email
     *
     * @param options
     * @return {Promise<boolean>}
     */
    public confirmAccountByEmail(options: ConfirmDto){

        let email = options.email.trim().toLowerCase();
        let token = options.token.trim();

        return this.repository.confirmUserByEmail(email,token);

    }

    /**
     * Confirms the account by phone
     *
     * @param options
     * @return {Promise<boolean>}
     */
    public confirmAccountByPhone(options: ConfirmDto){

        let phone = options.phone.trim();
        let token = options.token.trim();

        return this.repository.confirmUserByPhone(phone,token);

    }

    /**
     * Resets user Accounts (using email)
     * then publishes the @{EventTasks.EMAIL_CONFIRMATION_REQUEST} with new user object
     * to send confirmation email
     *
     * @param params
     * @return {Promise<boolean>}
     */
    public async requestAccountConfirmationEmail(params: PhoneEmailDto){

        //clean string
        const email = params.email.trim().toLowerCase();

        //refresh token on user repository
        let user = await this.repository.resetAccountByEmail(email);

        //Create the event and publish it
        let event = new EventTask(AccountConsumer.NAME, EventTasks.EMAIL_CONFIRMATION_REQUEST, user.toJSON());
        this.eventPublisher.publish(event);

    }


    /**
     * Resets user Accounts (using phone)
     * then publishes the @{EventTasks.PHONE_CONFIRMATION_REQUEST} with new user object
     * to send confirmation sms
     *
     * @param params
     * @return {Promise<boolean>}
     */
    public async requestAccountConfirmationPhone(params: PhoneEmailDto){

        //clean string
        const phone = params.phone.trim();

        //refresh token on user repository
        let user = await this.repository.resetAccountByPhone(phone);

        //Create the event and publish it
        let event = new EventTask(AccountConsumer.NAME, EventTasks.PHONE_CONFIRMATION_REQUEST, user.toJSON());
        this.eventPublisher.publish(event);

    }

    /**
     * Resets user Accounts (using email)
     * then publishes the @{EventTasks.EMAIL_RESET_PASSWORD_REQUEST} with new user object
     *
     * @param params
     * @return {Promise<boolean>}
     */
    public async requestAccountResetByEmail(params: PhoneEmailDto){

        //clean string
        const email = params.email.trim().toLowerCase();

        //refresh token on user repository
        let user = await this.repository.resetAccountByEmail(email);

        //Create the event and publish it
        let event = new EventTask(AccountConsumer.NAME, EventTasks.EMAIL_RESET_PASSWORD_REQUEST, user.toJSON());
        this.eventPublisher.publish(event);

    }



    /* Resets user Accounts (using phone)
     * then publishes the @{EventTasks.PHONE_RESET_PASSWORD_REQUEST} with new user object
     *
     * @param params
     * @return {Promise<boolean>}
     */
    public async requestAccountResetByPhone(params: PhoneEmailDto){

        //clean string
        const phone = params.phone.trim();

        //refresh token on user repository
        let user = await this.repository.resetAccountByPhone(phone);

        //Create the event and publish it
        let event = new EventTask(AccountConsumer.NAME, EventTasks.PHONE_RESET_PASSWORD_REQUEST, user.toJSON());
        this.eventPublisher.publish(event);

    }


    /**
     * Resets users password (using email)
     * with a new chosen one
     *
     * @param params
     * @return {Promise<>}
     */
    public async resetPasswordByEmail(params: ResetAccountDto){

        let token = params.token.trim();
        let email = params.email.trim().toLowerCase();
        let password = params.password.trim();

        return this.repository.resetPasswordByEmail(token, email, password);

    }


    /**
     * Resets user's password (using phone)
     * with a new chosen one
     *
     * @param params
     * @return {Promise<>}
     */
    public async resetPasswordByPhone(params: ResetAccountDto){

        let token = params.token.trim();
        let phone = params.phone.trim();
        let password = params.password.trim();

        return this.repository.resetPasswordByPhone(token, phone, password);

    }

    public async authenticateCredentials(params : CredentialDto){

        //lets create the credentials
        let email = params["email"].toLowerCase().trim();
        let password =  params["password"].trim();
        let userDoc = await this.repository.findUserByCredential(email, password);
        let signature =  {user : {_id : userDoc["_id"]}};


        //add functions of admin
        userDoc["functions"] = this.roleService.getAvailableFunctions(userDoc as User);

        //select only keys
        let user =  Utils.selectKeys(userDoc, "_id email name functions");

        //lets create the token
        let token =  await jwt.sign(signature, process.env.JWT_SECRET, {
            expiresIn: "60m",
            issuer : "riby:riby-api-sso"
        });

        return {user, token};

    }


}