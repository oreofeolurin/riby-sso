import {IsEmail, IsString, MinLength,  IsNumberString, ValidateIf} from "class-validator";


export class ConfirmDto{
    @ValidateIf( o => typeof o.phone == "undefined")
    @IsEmail()
    readonly email: string;

    @ValidateIf( o => typeof o.email == "undefined")
    @IsNumberString()
    readonly phone: string;

    @IsString()
    readonly token: string;
}


export class PhoneEmailDto{
    @ValidateIf( o => typeof o.phone == "undefined")
    @IsEmail()
    readonly email: string;

    @ValidateIf( o => typeof o.email == "undefined")
    @IsNumberString()
    readonly phone: string;
}



export class CredentialDto{
    @IsEmail()
    readonly email: string;

    @IsString()
    @MinLength(8)
    readonly password: string;

}


export class ResetAccountDto{
    @ValidateIf( o => typeof o.phone == "undefined")
    @IsEmail()
    readonly email: string;

    @ValidateIf( o => typeof o.email == "undefined")
    @IsNumberString()
    readonly phone: string;

    @IsString()
    @MinLength(8)
    readonly password: string;

    @IsString()
    readonly token: string;
}
