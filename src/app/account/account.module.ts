import {Module} from "@nestjs/common";
import {CoreModule} from "../core/core.module";
import {AccountController} from "./account.controller";
import {AccountService} from "./account.service";
import {AccountConsumer} from "./account.consumer";
import {UserModule} from "../api/user/user.module";
import {RoleModule} from "../api/role/role.module";

@Module({
    modules: [CoreModule, UserModule, RoleModule],
    controllers: [AccountController],
    components: [AccountService,  AccountConsumer],
    exports: [AccountService]
})
export class AccountModule{}