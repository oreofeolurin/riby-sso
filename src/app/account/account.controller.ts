import {Controller, Post, Body, HttpStatus, Req, Get, Param, Query, Res,} from '@nestjs/common';
import {ConfirmDto, CredentialDto, PhoneEmailDto, ResetAccountDto} from "./account.dto";
import {AccountService} from "./account.service";
import {CreateUserDto} from "../api/user/user.dto";

@Controller('/account')
export class AccountController {

    constructor(private readonly accountService: AccountService) {}

    @Post("/request/confirmation")
    public async requestAccountConfirmationByEmail(@Body() params: PhoneEmailDto) {

        if(params.email)
            await this.accountService.requestAccountConfirmationEmail(params);
        else
            await this.accountService.requestAccountConfirmationPhone(params);

        return {code: HttpStatus.OK, message: "Request Successful"};

    }



    @Post("/confirm")
    public async confirmAccountByEmail(@Body() params: ConfirmDto) {
        if(params.email)
            await this.accountService.confirmAccountByEmail(params);

        else
            await this.accountService.confirmAccountByPhone(params);

        return {code: HttpStatus.OK, message: "Request Successful"};
    }

    @Get("/confirm")
    public async confirmAccountByEmailGET(@Query() params: ConfirmDto, @Res() res) {

        try {

            if (params.email)
                await this.accountService.confirmAccountByEmail(params);

            else
                await this.accountService.confirmAccountByPhone(params);

            res.redirect(`${process.env.SITE_DOMAIN}/login?confirmed=true`);

        } catch (err){
            res.redirect(`${process.env.SITE_DOMAIN}/login?confirmed=false`);
        }
    }


    @Post("/request/reset/password")
    public async requestAccountResetByEmail(@Body() params: PhoneEmailDto) {
        if(params.email)
            await this.accountService.requestAccountResetByEmail(params);
        else
            await this.accountService.requestAccountResetByPhone(params);


        return {code: HttpStatus.OK, message: "Request Successful"};
    }


    @Post("/reset/password")
    public async resetPassword(@Body() params: ResetAccountDto) {
        if(params.email)
            await this.accountService.resetPasswordByEmail(params);
        else
            await this.accountService.resetPasswordByPhone(params);


        return {code: HttpStatus.OK, message: "Request Successful"};

    }

    /**
     * Creates User JWT Token to be used for authenticating server calls
     *
     * @param params
     */
    @Post("/create/user/token")
    public async createUserToken(@Body() params: CredentialDto) {

        let {user , token} = await this.accountService.authenticateCredentials(params);

        return {code: HttpStatus.OK, message: "Request Successful", body: {token, user}};

    }

}