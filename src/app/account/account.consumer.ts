import {Component} from "@nestjs/common";
import {EventPublisher} from "../core/events/event.publisher";
import {JobService} from "../core/services/job.service";
import {EmailJob} from "../core/jobs/email-job";
import {AppMessageSender, SMSTemplates} from "../core/helpers/constants";
import {EventTask, EventTasks} from "../core/events/event-task";
import {SMSJob} from "../core/jobs/sms.job";
import {EmailTemplates, QueueTasks} from "../core/helpers/enums";
import {Consumer} from "../core/abstracts/consumer";
import {ShorterService} from "../core/services/shorter.service";

/**
 * Implementation of a {@link Consumer} class for the {@link AccountModule}
 *
 * @author  Oreofe Olurin
 * @version 0.0.1
 * @since   2017-10-20
 */
@Component()
export class AccountConsumer extends Consumer {

    public static NAME = "ACCOUNT_CONSUMER";

    constructor(private jobService: JobService, private shorterService: ShorterService, eventPublisher: EventPublisher) {
        super(AccountConsumer.NAME, eventPublisher);
    }

    public handleEvent(event: EventTask) {

        switch (event.name) {
            case EventTasks.USER_CREATED:
                this.handleSavedEvent(event);
                break;

            case EventTasks.MEMBER_CREATED:
                this.handleMemberCreatedEvent(event);
                break;

            case EventTasks.ADMIN_CREATED:
                this.handleAdminCreatedEvent(event);
                break;

            case EventTasks.EMAIL_CONFIRMATION_REQUEST:
                this.handleEmailConfirmationRequest(event);
                break;

            case EventTasks.PHONE_CONFIRMATION_REQUEST:
                this.handleSMSConfirmationRequest(event);
                break;

            case EventTasks.EMAIL_RESET_PASSWORD_REQUEST:
                this.handleEmailResetPasswordRequest(event);
                break;
            case EventTasks.PHONE_RESET_PASSWORD_REQUEST:
                this.handleSMSResetPasswordRequest(event);
                break;
        }
    }

    /**
     * Sends confirmation email to user if email is present in data,
     * else sends an SMS confirmation to phone
     *
     * @param {EventTask} event
     */
    private handleSavedEvent(event: EventTask) {


        if ((<any>event.data).email)
            return this.handleEmailConfirmationRequest(event);

        return this.handleSMSConfirmationRequest(event);

    }


    /**
     * Sends welcome email to member user if email is present in data,
     * else sends a welcome SMS  to phone
     *
     * @param {EventTask} event
     */
    private handleMemberCreatedEvent(event: EventTask) {

        if ((<any>event.data).user.email)
            return this.handleEmailMemberCreated(event);

        return this.handleSMSMemberCreated(event);

    }


    /**
     * Sends welcome email to admin user if email is present in data,
     * else sends a welcome SMS  to phone
     *
     * @param {EventTask} event
     */
    private handleAdminCreatedEvent(event: EventTask) {

        if ((<any>event.data).user.email)
            return this.handleEmailAdminCreated(event);

        return this.handleSMSAdminCreated(event);

    }


    /**
     * Builds email job for welcoming members
     * and adds to @{WorkerQueues.PROCESS_EMAIL} queue for processing
     *
     * @param {EventTask} event
     */
    private async handleEmailMemberCreated(event: EventTask) {

        let email = event.data["user"].email;
        let token = event.data["user"].token.email.value;
        let cooperativeName = event.data["cooperative"].name;
        
        let resetLink =
            await this.shorterService.shorten(`${process.env.SITE_DOMAIN}/reset?email=${email}&token=${token}`);

        
        let additionalData = {reset_link: resetLink, cooperativeName: cooperativeName};


        this.handleEmail(event.data["user"], 'Welcome to Riby', EmailTemplates.MEMBER_WELCOME, additionalData);

    }


    /**
     * Builds email job for welcoming members
     * and adds to @{WorkerQueues.PROCESS_EMAIL} queue for processing
     *
     * @param {EventTask} event
     */
    private async handleEmailAdminCreated(event: EventTask) {

        let email = event.data["user"].email;
        let token = event.data["user"].token.email.value;
        let cooperativeName = event.data["cooperative"].name;

        let resetLink =
            await this.shorterService.shorten(`${process.env.SITE_DOMAIN}/reset?email=${email}&token=${token}`);

        let additionalData = {reset_link: resetLink, cooperativeName: cooperativeName};


        this.handleEmail(event.data["user"], 'Welcome to Riby', EmailTemplates.ADMIN_WELCOME, additionalData);

    }

    /**
     * Builds SMS job for welcoming members
     *
     * @param {EventTask} event
     */
    private async handleSMSMemberCreated(event: EventTask) {

        let firstName = event.data["user"].name.first;
        let token = event.data["user"].token.phone.value;
        let phone = event.data["user"].phone;
        let cooperativeName = event.data["cooperative"].name;
        
        let resetLink =
            await this.shorterService.shorten(`${process.env.SITE_DOMAIN}/reset?phone=${phone}&token=${token}`);

        this.handleSMS(event.data["user"], SMSTemplates.MEMBER_WELCOME(firstName, cooperativeName, resetLink));

    }


    /**
     * Builds SMS job for welcoming members
     *
     * @param {EventTask} event
     */
    private async handleSMSAdminCreated(event: EventTask) {

        let firstName = event.data["user"].name.first;
        let token = event.data["user"].token.phone.value;
        let phone = event.data["user"].phone;
        let cooperativeName = event.data["cooperative"].name;
        
        let resetLink =
            await this.shorterService.shorten(`${process.env.SITE_DOMAIN}/reset?phone=${phone}&token=${token}`);


        this.handleSMS(event.data["user"], SMSTemplates.ADMIN_WELCOME(firstName, cooperativeName, resetLink));

    }


    /**
     * Builds email job for confirming account
     * and adds to @{WorkerQueues.PROCESS_EMAIL} queue for processing
     *
     * @param {EventTask} event
     */
    private async handleEmailConfirmationRequest(event: EventTask) {

        let email = event.data['email'];
        let token = event.data["token"].email.value;

        let verifyLink =
            await this.shorterService.shorten(`${process.env.API_DOMAIN}/v1/account/confirm?email=${email}&token=${token}`);

        this.handleEmail(event.data, 'Welcome to Riby', EmailTemplates.USER_WELCOME, {verify_link: verifyLink});

    }


    /**
     * Handle sms confirmation
     *
     * @param {EventTask} event
     */
    private async handleSMSConfirmationRequest(event: EventTask) {

        let token = event.data["token"].phone.value;
        let phone = event.data["phone"];

        let verifyLink =
            await this.shorterService.shorten(`${process.env.API_DOMAIN}/v1/account/confirm?phone=${phone}&token=${token}`);

        this.handleSMS(event.data, SMSTemplates.USER_WELCOME(verifyLink));

    }


    /**
     * Handle email resetting password
     *
     * @param {EventTask} event
     */
    private async handleEmailResetPasswordRequest(event: EventTask) {

        let email = event.data['email'];
        let token = event.data['token'].email.value;

        let resetLink =
            await this.shorterService.shorten(`${process.env.SITE_DOMAIN}/reset?email=${email}&token=${token}`);


        this.handleEmail(event.data, 'Riby Password Reset', EmailTemplates.RESET_PASSWORD, {reset_link: resetLink});
    }


    /**
     * Handle email resetting password
     *
     * @param {EventTask} event
     */
    private async handleSMSResetPasswordRequest(event: EventTask) {

        let token = event.data["token"].phone.value;
        let phone = event.data["phone"];

        let resetLink = await this.shorterService.shorten(`${process.env.SITE_DOMAIN}/reset?phone=${phone}&token=${token}`);

        this.handleSMS(event.data, SMSTemplates.RESET_PASSWORD(resetLink));
    }


    /**
     * Builds email job
     * and adds to @{WorkerQueues.PROCESS_EMAIL} queue for processing
     *
     * @param {EventTask} user
     * @param {string} subject
     * @param {string} template
     * @param additionalContent
     */
    private handleEmail(user: any, subject: string, template: string, additionalContent: object = {}) {

        let defaultContent = {
            name: user.name,
            token: user.token.email.value,
            static_domain: process.env.STATIC_DOMAIN,
            site_domain: process.env.SITE_DOMAIN
        };


        let emailJob = new EmailJob()
            .setFrom({email: "no-reply@riby.me", name: AppMessageSender})
            .setTo({email: user.email, name: user.name.full})
            .setSubject(subject)
            .setTemplate(template)
            .setContent(Object.assign(defaultContent, additionalContent));

        this.jobService.addJobToQueue(emailJob, QueueTasks.SEND_EMAIL);

    }


    /**
     * Builds SMS job
     * and adds to {@link QueueTasks.SEND_SMS} task for processing
     *
     * @param {User} user
     * @param {string} template
     */
    private handleSMS(user: any, template: string) {

        //lets send SMS to job queue
        let smsJob = new SMSJob()
            .setFrom(AppMessageSender)
            .setTo(user.phone)
            .setBody(template);


        this.jobService.addJobToQueue(smsJob, QueueTasks.SEND_SMS);

    }


}