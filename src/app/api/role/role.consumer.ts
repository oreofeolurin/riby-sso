import {Component, forwardRef, Inject} from "@nestjs/common";
import {Subscription} from "rxjs";
import {RoleService} from "./role.service";
import {EventPublisher} from "../../core/events/event.publisher";
import {EventTask, EventTasks} from "../../core/events/event-task";
import {IEvent} from "../../core/events/event.interface";


@Component()
export class RoleConsumer{

    public static NAME =  "ROLE_CONSUMER";

    private eventSubscription: Subscription;

    constructor(@Inject(forwardRef(() => RoleService)) private readonly roleService: RoleService,
                readonly eventPublisher: EventPublisher) {

        let filteredStream = eventPublisher.eventStreams.filter((event: IEvent) => event.consumer == RoleConsumer.NAME );

        this.eventSubscription = filteredStream.subscribe(event => this.handleEvent(event));

    }

    private handleEvent(event: EventTask) {

        switch (event.name){
            case EventTasks.ROLES_AND_FUNCTIONS_CHANGED:
                this.handleSavedEvent(event);
                break;
        }
    }

    /**
     * Sends confirmation email to user if email is present in data,
     * else sends an SMS confirmation to phone
     *
     * @param {EventTask} event
     */
    private handleSavedEvent(event: EventTask){
        this.roleService.loadRolesToCache();
    }

}