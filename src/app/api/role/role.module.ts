import {forwardRef, Module} from "@nestjs/common";
import {RoleController} from "./role.controller";
import {RoleRepository} from "./role.repository";
import {RoleModel} from "./role.model";
import {RoleConsumer} from "./role.consumer";
import {RoleService} from "./role.service";
import {CoreModule} from "../../core/core.module";
import {UserModule} from "../user/user.module";

@Module({
    modules: [CoreModule, forwardRef(() => UserModule)],
    controllers: [RoleController],
    components: [RoleService, RoleConsumer, RoleModel, RoleRepository],
    exports: [RoleService],
})
export class RoleModule{}