import {Component} from "@nestjs/common";
import {CreateRoleDto, RoleFunctionsDto} from "./role.dto";
import {EventTask, EventTasks} from "../../core/events/event-task";
import {RoleConsumer} from "./role.consumer";
import {RoleRepository} from "./role.repository";
import {EventPublisher} from "../../core/events/event.publisher";
import {Role} from "./role";
import * as mongoose from "mongoose";
import {Utils} from "../../core/helpers/utils";
import {User} from "../user/user.interface";

type Cache = {name: Map<string, Role>, id: Map<string, Role>};
@Component()
export class RoleService {

    private cache: Cache = {name: new Map(), id: new Map()};

    constructor(private readonly repository: RoleRepository,
                private readonly eventPublisher: EventPublisher){
        this.loadRolesToCache();
    }

    public async loadRolesToCache(){

        //clear cache
        this.cache.name.clear();
        this.cache.id.clear();

        //get all from repository
        let roles = await this.repository.findAll();

        //set the role-functions, index name and id
        roles.forEach((role) => {
            this.cache.name.set(role["name"], <any>role);
            this.cache.id.set(role["_id"].toHexString(), <any>role);
        });

    }

    public getFromCacheByName(roleName: string): Role{
        return this.cache.name.get(roleName);
    }


    /**
     * Gets all available functions of a admin
     *
     * @param {User} user
     * @returns {Array<string>}
     */
    public getAvailableFunctions(user: User): Array<string>{

        const functions = Array.isArray(user.functions) ?user.functions : [];
        const roleFunctions = Array.isArray(user.roles) && this.getRoleFunctions(user.roles);


        return roleFunctions ? functions.concat(roleFunctions) : functions;

    }

    /**
     * Get all role functions of a given array of role
     *
     * @param {Array<"mongoose".Types.ObjectId>} roles
     * @returns {Array}
     */
    public getRoleFunctions(roles: Array<mongoose.Types.ObjectId>){

        let functions = [];

        roles.forEach(role => {
            let cachedRole = this.getFromCacheById(role);
            if (cachedRole && cachedRole.functions)
                functions = functions.concat(cachedRole.functions);
        });

        return functions;
    }


    public getFromCacheById(roleId: mongoose.Types.ObjectId): Role{
        return roleId instanceof mongoose.Types.ObjectId && this.cache.id.get(roleId.toHexString());
    }



    public async create(params:  CreateRoleDto){

        //clean data
        let details = {
            name: params.name.trim().replace(/\s/g,''),
            functions: params.functions
        };

        //save to repository
        let role = await this.repository.create(details);

        this.emitChanged();

        return role;
    }


    public async addFunctions(roleName: string, params:  RoleFunctionsDto){

        await this.repository.addFunctionsByName(roleName, params.functions);
        this.emitChanged();
    }


    public async removeFunctions(roleName: string, params:  RoleFunctionsDto){

        await this.repository.removeFunctionsByName(roleName, params.functions);
        this.emitChanged();
    }


    public async edit(roleName: string, params:  CreateRoleDto){

        //clean data
        let updateObject = {
            functions: params.functions,
            name: params.name && params.name.trim()
        };

        updateObject = Utils.removeNilValues(updateObject);

        //save to repository
        await this.repository.updateByName(roleName, updateObject);
        this.emitChanged();

    }

    public async delete(roleName: string){

        //delete from this.repository
        await this.repository.deleteByName(roleName);
        this.emitChanged();

    }

    public async findRole(roleName: string){

        //get from repository
        return await this.repository.findOneByName(roleName);
    }

    public async findAll(){

        //get all from repository
        return await this.repository.findAll();
    }


    /**
     * Create the event and publish it
     */
    private emitChanged(){
        let event = new EventTask(RoleConsumer.NAME, EventTasks.ROLES_AND_FUNCTIONS_CHANGED);
        this.eventPublisher.publish(event);
    }

}