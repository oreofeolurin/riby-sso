import {Body, Controller, HttpStatus, Post, UseGuards, Req, Put, Param, Delete, Get} from "@nestjs/common";
import {RoleService} from "./role.service";
import {CreateRoleDto, RoleFunctionsDto} from "./role.dto";
import {RoleFunctions, RolesGuard} from "./roles.guard";
import {RolesFunctions} from "./role";
import {ApiGuard} from "../api.guard";

@Controller('/role')
@UseGuards(ApiGuard, RolesGuard)
export class RoleController{

    constructor(private readonly roleService: RoleService){}

    @Post("/")
    @RoleFunctions(RolesFunctions.CREATE_ROLES_AND_FUNCTIONS)
    public async createRole(@Req() req, @Body() params: CreateRoleDto) {

        let role = await <any>this.roleService.create(params);

        return {
            code: HttpStatus.CREATED,
            message : "Successfully Created",
            body : {name: role.name, functions: role.functions}
        };
    }


    @Get("/")
    @RoleFunctions(RolesFunctions.READ_ROLES_AND_FUNCTIONS)
    public async findAllRoles() {

        let roles = await this.roleService.findAll();

        return {code: HttpStatus.CREATED, message : "Successfully Created", body : roles};
    }

    @Get("/:roleName")
    @RoleFunctions(RolesFunctions.READ_ROLES_AND_FUNCTIONS)
    public async findRole(@Param() params) {

        let role = await this.roleService.findRole(params.roleName);

        return {code: HttpStatus.CREATED, message : "Successfully Created", body : role};
    }


    @Put("/:roleName")
    @RoleFunctions(RolesFunctions.UPDATE_ROLES_AND_FUNCTIONS)
    public async editRole(@Body() body: CreateRoleDto, @Param() params) {

        await this.roleService.edit(params.roleName, body);

        return {code: HttpStatus.OK, message : "Request Successful"};
    }


    @Delete("/:roleName")
    @RoleFunctions(RolesFunctions.DELETE_ROLES_AND_FUNCTIONS)
    public async deleteRole(@Param() params) {

        await this.roleService.delete(params.roleName);

        return {code: HttpStatus.OK, message : "Request Successful"};
    }

    @Put("/:roleName/functions")
    @RoleFunctions(RolesFunctions.UPDATE_ROLES_AND_FUNCTIONS)
    public async addRoleFunctions(@Param() params, @Body() body: RoleFunctionsDto) {

        await this.roleService.addFunctions(params.roleName, body);

        return {code: HttpStatus.OK, message : "Request Successful"};
    }


    @Delete("/:roleName/functions")
    @RoleFunctions(RolesFunctions.DELETE_ROLES_AND_FUNCTIONS)
    public async removeRoleFunctions(@Param() params, @Body() body: RoleFunctionsDto) {

        await this.roleService.removeFunctions(params.roleName, body);

        return {code: HttpStatus.OK, message : "Request Successful"};
    }


}