import {IsArray, IsOptional, IsString, ValidateIf} from "class-validator";
import * as mongoose from "mongoose";

export class CreateRoleDto{
    @IsString()
    @IsOptional()
    name: string;

    @IsArray()
    functions: Array<string>
}

export class RoleFunctionsDto{
    @IsArray()
    functions: Array<string>
}

export class RolesFunctionsDto {
    @IsArray()
    @ValidateIf( o => typeof o.functions == "undefined")
    readonly roles: Array<mongoose.Types.ObjectId>;

    @IsArray()
    @ValidateIf( o => typeof o.roles == "undefined")
    readonly functions: Array<mongoose.Types.ObjectId>;
}