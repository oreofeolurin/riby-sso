import {Guard, CanActivate, ExecutionContext} from '@nestjs/common';
import { Reflector} from '@nestjs/core';
import { ReflectMetadata } from '@nestjs/common';
import {RoleService} from "./role.service";

export const RoleFunctions = (...roles: string[]) => ReflectMetadata('functions', roles);

@Guard()
export class RolesGuard implements CanActivate {

    constructor(private readonly reflector: Reflector,
                private roleService: RoleService) {
    }

    canActivate(req, context: ExecutionContext): boolean {

        const {parent, handler} = context;
        const requiredFunctions = this.reflector.get<string[]>('functions', handler);
        if (!requiredFunctions) {
            return true;
        }

        const user = req.locals.user;
        const availableFunctions = user && this.roleService.getAvailableFunctions(user);

        const hasFunction = () => !!availableFunctions.find((func) => !!requiredFunctions.find((item) => item === func));

        return user && availableFunctions && hasFunction();
    }

}