/**
 * Created by EdgeTech on 12/15/2016.
 */
import * as mongoose from 'mongoose';
import {Component} from "@nestjs/common";
import {MongodbHandle} from "../../core/connections/mongodb-handle";

@Component()
export class RoleModel {
    private model: mongoose.Model<mongoose.Document>;

    constructor(private readonly mongodb: MongodbHandle) {
        this.registerModel();
    }

    /**
     * Gets the mongoose Document
     *
     * @return {mongoose.Model<mongoose.Document>}
     */
    public getModel(): mongoose.Model<mongoose.Document> {
        return this.model;
    }

    /**
     * Creates the mongoose model
     */
    private registerModel() {
        //create the Model
        let model = RoleModel.createModel(this.mongodb.getHandle());
        this.setModel(model);
    }

    /**
     * Set the mongoose model
     */
    public setModel(model: mongoose.Model<mongoose.Document>) {
        this.model = model;
    }


    /**
     * Create the mongoose model using the DBConnection
     *
     * @param DBConnection
     * @return {mongoose.Model<mongoose.Document>}
     */
    public static createModel(DBConnection: mongoose.Connection): mongoose.Model<mongoose.Document> {

        //create schema
        let schema = this.createSchema();

        return DBConnection.model('Role', schema);


    }

    private static createSchema(): mongoose.Schema {

        let schema = new mongoose.Schema({
            name: {
                index: true,
                unique: true,
                type: String,
                required: true
            },
            functions: [{type: String}]

        }, {'timestamps': true});

        schema.set('toJSON', {getters: true, virtuals: true});

        return schema;

    }

}