import * as mongoose from "mongoose";
import {Component} from "@nestjs/common";
import {RoleModel} from "./role.model";
import {Role} from "./role";
import {AppException} from "../../core/exceptions/app.exception";
import {MongodbException} from "../../core/exceptions/mongodb.exception";

@Component()
export class RoleRepository {
    private roleModel: mongoose.Model<mongoose.Document>;

    constructor(model: RoleModel) {
        this.roleModel = model.getModel();
    }


    public create(details: Role) {

        //Lets create a skeleton user with provided form params
        let app = new this.roleModel(details);

        return this.save(app);
    }


    public updateByName(name: string, updateObject: object) {
        return this.update({name: name}, { $set : updateObject});
    }


    public addFunctionsByName(name: string, array: Array<any>) {
        return this.update({name: name}, { $addToSet : { functions : {$each : array}}});
    }

    public removeFunctionsByName(name: string, array: Array<any>) {

        return this.update({name: name}, { $pull: { functions: { $in: array } } });
    }


    public deleteByName(name: string) {
        return this.remove({name : name});
    }

    public findAll() {
        return this.find({});
    }

    public findOneByName(name: string) {
        return this.findOne({name : name});
    }



    /**
     * Saves an app in DB
     *
     * @param app
     * @return {Promise<Object>}
     */
    private save(app): Promise<Object>{

        return new Promise<Object>(function(resolve,reject) {

            app.save((err, doc) => {
                if (err) return reject(new MongodbException(err));

                return resolve(doc.toJSON());
            });
        })

    }


    /**
     * Gets all apps in the database
     */
    public find(conditions: object): Promise<Array<object>>{

        return new Promise((resolve, reject) => {

            this.roleModel
                .find(conditions)
                .select("name functions")
                .lean()
                .exec(function (err, docs: Array<mongoose.Document>) {

                    if (err) return reject(new Error(err));

                    return resolve(docs);

                });

        })

    }


    /**
     * Gets one app in the database
     */
    public findOne(conditions: object){

        return new Promise((resolve, reject) => {

            this.roleModel
                .findOne(conditions)
                .select("name functions")
                .lean()
                .exec(function (err, doc: mongoose.Document) {

                    if (err) return reject(new Error(err));

                    if(!doc)
                        return reject(AppException.UNKNOWN_ROLE);

                    return resolve(doc);

                });

        })

    }


    /**
     * Updates an existing app
     *
     * @param conditions
     * @param update
     * @returns {Promise<string>}
     */
    private update(conditions: object, update: object){

        return new Promise((resolve, reject)  => {
            this.roleModel.update(conditions, update).exec((err, raw)=> {
                if (err)
                    return reject(new MongodbException(err));


                if (raw.nModified < 1)
                    return reject(AppException.UNKNOWN_ROLE);

                return resolve(true);
            });
        })
    }


    private remove(conditions: object){

        return new Promise((resolve, reject) => {

            this.roleModel.remove(conditions)
                .exec((err)=> {
                    if (err) return reject(new MongodbException(err));

                    return resolve(true);
                });
        })
    }



}