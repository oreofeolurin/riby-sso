import {Module} from "@nestjs/common";
import {UserRepository} from "./user.repository";
import {UserModel} from "./user.model";
import {CoreModule} from "../../core/core.module";
import {UserController} from "./user.controller";
import {UserService} from "./user.service";
import {RoleModule} from "../role/role.module";


@Module({
    modules: [CoreModule, RoleModule],
    controllers: [UserController],
    components: [UserRepository, UserModel, UserService],
    exports: [UserRepository, UserModel]
})
export class UserModule{}
