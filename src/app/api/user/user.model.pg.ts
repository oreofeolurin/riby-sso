/**
 * Created by EdgeTech on 12/15/2016.
 */
import * as crypto from 'crypto';
import * as Sequelize from "sequelize";
import {Component} from "@nestjs/common";
import {PostgresDBHandle} from "../../core/connections/postgresdb-handle";
import {ModelAttributes, ModelInstance} from "../../core/abstracts/model";
import {ACCOUNT_STATUS_TYPES} from "../../core/helpers/constants";

interface UserAttributes extends ModelAttributes{
    password: string,
    salt: string
    status: boolean
}
interface UserSequelize extends UserAttributes, ModelInstance<UserAttributes>{}


@Component()
export class UserModel{
    private model: UserSequelize;

    constructor(private readonly postgresdb: PostgresDBHandle){
        this.registerModel();
    }

    /**
     * Gets the sequelize
     *
     * @return {UserSequelize}
     */
    public getModel(){
        return this.model;
    }

    /**
     * Creates the sequelize model
     */
    private registerModel(){
        let model = UserModel.createModel(this.postgresdb.getHandle());
        this.setModel(model);
    }

    /**
     * Set the sequelize model
     */
    public setModel(model){
        this.model = model;
    }



    /**
     * Create the sequelize model
     *
     * @param sequelize
     * @return {}
     */
    public static createModel(sequelize: Sequelize.Sequelize) {

        //create schema
        let attributes = this.createAttributes();
        let hooks = this.createHooks();

        return  sequelize.define<UserSequelize, UserAttributes>('user',attributes, {hooks});

    }

    private static createAttributes(): Sequelize.DefineAttributes {

        return  {

            firstName: {
                allowNull: false,
                type: Sequelize.STRING
            },


            lastName: {
                allowNull: false,
                type: Sequelize.STRING
            },



           email: {
               allowNull: false,
               type: Sequelize.STRING,
               unique: true,
               validate: {
                   isEmail: true
               },
           },


           phone : {
               allowNull: false,
               unique: true,
               type: Sequelize.STRING
           },

            password: {
                allowNull: false,
                type: Sequelize.STRING,
                unique: true,
                validate: {
                    min : {
                        args : 6,
                        msg : 'Password should be longer'
                    }
                }
            },

            salt: {
                allowNull: false,
                type: Sequelize.STRING,
                unique: true,
            },

            confirmed: {
                allowNull: false,
                type: Sequelize.JSON,
            },

            token:{
                allowNull: false,
                type: Sequelize.JSON
            },

            status: {
                defaultValue: ACCOUNT_STATUS_TYPES.PENDING,
                type: Sequelize.STRING
            },

          roles: Sequelize.ARRAY(Sequelize.STRING),


          functions: Sequelize.ARRAY(Sequelize.STRING)
        };

    }


    private static createHooks(): Sequelize.HooksDefineOptions<UserSequelize> {
        return {
            beforeValidate : ((user, options) => {
                user.salt = new Buffer(crypto.randomBytes(16).toString('base64'), 'base64').toString('hex')
            }),

            beforeCreate : ((user, options) => {
                user.password = UserModel.hashPassword(user.password,user.salt);
            }),
        }
    }

    //Define hashPassword method
   public static hashPassword(password: string, salt: string) {
        return crypto.pbkdf2Sync(password, salt, 10000, 64, "sha512").toString('base64');
    }

}

/*
CREATE TABLE users (
    user_id serial  PRIMARY KEY,
    first_name varchar NOT NULL,
    last_name varchar NOT NULL,
    created_at TIMESTAMP NOT NULL,
    updated_at TIMESTAMP NOT NULL
)*/
