import {Component} from "@nestjs/common";
import {UserRepository} from "./user.repository";
import {CreateUserWithRolesAndFunctionsDto, UpdateUserDto} from "./user.dto";
import {Utils} from "../../core/helpers/utils";
import {EventTask, EventTasks} from "../../core/events/event-task";
import {AccountConsumer} from "../../account/account.consumer";
import {EventPublisher} from "../../core/events/event.publisher";
import {RolesFunctionsDto} from "../role/role.dto";

@Component()
export class UserService{

    constructor(
        private readonly repository: UserRepository,
        private readonly eventPublisher: EventPublisher){}


    /**
     *
     * @param {CreateUserWithRolesAndFunctionsDto} body
     * @returns {Promise<User>}
     */
    public async createUserWithRolesAndFunctions(body:  CreateUserWithRolesAndFunctionsDto){

        //lets see if the phone is a valid phone
        let phoneNumber =  await Utils.getPhoneE164(body.phone.trim());

        //clean data
        let userDetails = {
            name : {full : body.firstName.trim() + " " + body.lastName.trim()},
            phone: phoneNumber,
            email: body.email.trim().toLowerCase(),
            password : body.password.trim(),
            roles : body.roles,
            functions :  body.functions
        };

        userDetails =  Utils.removeNilValues(userDetails) as any;

        //save to repository
        let user = await this.repository.createUser(userDetails);

        //Create the event and publish it
        let event = new EventTask(AccountConsumer.NAME, EventTasks.USER_CREATED, user);
        this.eventPublisher.publish(event);

        return user;

    }


    public async update(userId: string, body: UpdateUserDto){

        //clean data
        let updateObject = {
            name : {full : body.firstName.trim() + " " + body.lastName.trim()},
        };

        //save to repository
        return await this.repository.updateById(userId, updateObject);

    }


    public async addRoleFunctions(adminId: string, body:  RolesFunctionsDto){

        //clean data
        let roles  = body.roles || [],  functions = body.functions || [];

        //save to repository
        return await this.repository.addRolesFunctions(adminId, roles, functions)
    }




}