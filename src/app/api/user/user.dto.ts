import {
    IsAlpha, IsArray, IsEmail, IsMongoId, IsNumberString, IsOptional, IsString, MaxLength, MinLength,
    ValidateIf
} from "class-validator";
import * as mongoose from "mongoose";



export class CreateUserDto{
    @IsAlpha()
    @MaxLength(20)
    readonly firstName: string;

    @IsAlpha()
    @MaxLength(20)
    readonly lastName: string;

    @ValidateIf( o => typeof o.email !== "undefined")
    @IsEmail()
    @MaxLength(60)
    readonly email: string;

    @IsNumberString()
    readonly phone: string;

    @IsString()
    @MinLength(8)
    readonly password: string;
}


export class CreateUserWithoutPasswordDto{
    @IsAlpha()
    @MaxLength(20)
    readonly firstName: string;

    @IsAlpha()
    @MaxLength(20)
    readonly lastName: string;

    @ValidateIf( o => typeof o.email !== "undefined")
    @IsEmail()
    @MaxLength(60)
    readonly email: string;

    @IsNumberString()
    readonly phone: string;
}


export class UpdateUserDto{

    @IsAlpha()
    @MaxLength(20)
    @IsOptional()
    readonly firstName: string;

    @IsAlpha()
    @MaxLength(20)
    @IsOptional()
    readonly lastName: string;
}


export class UserIdDto{
    @IsMongoId()
    userId: string;
}


export class CreateUserWithRolesAndFunctionsDto extends CreateUserDto{

    @IsArray()
    @IsOptional()
    readonly roles: Array<mongoose.Types.ObjectId>;

    @IsArray()
    @IsOptional()
    readonly functions: Array<string>;

}