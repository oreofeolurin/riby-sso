import * as mongoose from "mongoose";
import {Component} from "@nestjs/common";
import {User, UserWithoutPass} from "./user.interface";
import {UserModel} from "./user.model";
import {MongodbException} from "../../core/exceptions/mongodb.exception";
import {Utils} from "../../core/helpers/utils";
import {AppException} from "../../core/exceptions/app.exception";
import {ACCOUNT_STATUS_TYPES} from "../../core/helpers/constants";
import {Repository} from "../../core/abstracts/repository";

@Component()
export class UserRepository extends Repository{
   
    constructor(userModel: UserModel){
        super(userModel);
    }


    /**
     * Creates the user model the set the hashed password on the user,
     * using mongoose static function @{setHashedPassword(password)}
     * which is defined in @{UserModel}
     *
     * Then set token values on the email and phone fields to faciliate
     * confirmation of account
     *
     * @param {User} userDetails
     * @returns {Promise<Object>}
     */
    public createUser(userDetails: User) {

        //Lets create a skeleton user with provided form params
        let user = new this.model(userDetails);

        //set the hashed password;
        user["setHashedPassword"](user["password"]);


        //add token.value field for phone
        user['token'].phone.value = Utils.generateRandomNumber(6);
        user['token'].phone.created = new Date();


        //add token.value field for email if email is present
        if(user['email']) {
            user['token'].email.value = Utils.generateRandomNumber(6);
            user['token'].email.created = new Date();
        }

        return this.save(user);
    }



    /**
     * Creates the user model without a password on the user
     *
     * Then set token values on the email and phone fields to faciliate
     * confirmation of account
     *
     * @param {User} userDetails
     * @returns {Promise<Object>}
     */
    public createUserWithoutPassword(userDetails: UserWithoutPass) {

        //Lets create a skeleton user with provided form params
        let user = new this.model(userDetails);


        //add token.value field for phone
        user['token'].phone.value = Utils.generateRandomNumber(6);
        user['token'].phone.created = new Date();


        //add token.value field for email if email is present
        if(user['email']) {
            user['token'].email.value = Utils.generateRandomNumber(6);
            user['token'].email.created = new Date();
        }


        return this.save(user);
    }


    /**
     * Validate the token provided for the email,
     * then set confirm for user as true
     * next activates the use by updating its status
     *
     * @param {string} email
     * @param {string} token
     * @returns {Promise<Object>}
     */
    public async confirmUserByEmail(email: string, token: string){

        //lets first validate the token on the user
        let user = await this.validateEmailToken(email, token);

        //now lets confirm the user
        user['confirmed'].email = true;

        //activate account
        user['status'] = ACCOUNT_STATUS_TYPES.ACTIVE;

        //save user
        return this.save(user);

    }

    /**
     * Validate the token provided for the phone,
     * then set confirm for user as true
     * next activates the use by updating its status
     *
     * @param {string} phone
     * @param {string} token
     * @returns {Promise<Object>}
     */
    public async confirmUserByPhone(phone: string, token: string){

        //lets first validate the token on the user
        let user = await this.validatePhoneToken(phone, token);

        //now lets confirm the user
        user['confirmed'].phone = true;

        //activate account
        user['status'] = ACCOUNT_STATUS_TYPES.ACTIVE;

        //save user
        return this.save(user);

    }


    /**
     * Refreshes the token provided for the email,
     * then suspends the account
     *
     * @param {string} email
     * @returns {Promise<Object>}
     */
    public async resetAccountByEmail(email: string) {

        //lets first validate the token on the user
        let user = await this.refreshEmailToken(email);


        //suspend account
        user['status'] = ACCOUNT_STATUS_TYPES.SUSPENDED;

        //save user
        return this.save(user);

    }

    /**
     * Refreshes the token provided for the phone,
     * then suspends the account
     *
     * @param {string} phone
     * @returns {Promise<Object>}
     */
    public async resetAccountByPhone(phone: string) {

        //lets first validate the token on the user
        let user = await this.refreshPhoneToken(phone);


        //suspend account
        user['status'] = ACCOUNT_STATUS_TYPES.SUSPENDED;

        //save user
        return this.save(user);

    }


    /**
     * Validate the token provided for the email,
     * then sets the hashed password on the user
     *
     * @param email
     * @param token
     * @param password
     * @return {Promise<object>}
     */
    public async resetPasswordByEmail(token: string, email: string, password: string): Promise<object>{

        //lets first validate the token on the user
        let user = await this.validateEmailToken(email, token);

        //lets now set new password
        user["setHashedPassword"](password);

        //save user
        return this.save(user);

    }


    /**
     * Validate the token provided for the phone,
     * then sets the hashed password on the user
     *
     * @param email
     * @param token
     * @param password
     * @return {Promise<object>}
     */
    public async resetPasswordByPhone(token: string, email: string, password: string): Promise<object>{

        //lets first validate the token on the user
        let user = await this.validatePhoneToken(email, token);

        //lets now set new password
        user["setHashedPassword"](password);

        //save user
        return this.save(user);

    }


    /**
     * Check if token is present on user(using email)
     * and it is not expired
     *
     * @param {string} email
     * @param {string} token
     * @returns {Promise<Object>}
     */
    private validateEmailToken(email: string, token: string) : Promise<mongoose.Document> {

        const self = this;

        return new Promise(function(resolve,reject) {
            self.model
                .findOne({'email': email})
                .select('token confirmed')
                .exec(function (err, user) {

                    if(err) return reject(new MongodbException(err));

                    if(!user) return reject(AppException.UNKNOWN_USER);

                    if(user['token'].email.value != token) return reject(AppException.INVALID_OTP);

                    //lets make sure it hasn't expired
                    let createdDate = user['token'].email.created;
                    let currentDate = new Date();

                    if(Utils.daysBetween(currentDate,createdDate) > 1 )
                        return Promise.reject(AppException.EXPIRED);

                    //Okay now it hasn't expires so lets do house cleaning
                    user['token'].email.value = null;
                    user['token'].email.created = null;


                    //make sure the account is confirmed
                    user['confirmed'].email = true;

                    resolve(user);
                })

        })
    }


    /**
     * Check if token is present on user(using phone)
     * and it is not expired
     *
     * @param {string} phone
     * @param {string} token
     * @returns {Promise<Object>}
     */
    private validatePhoneToken(phone: string, token: string): Promise<mongoose.Document>{

        const self = this;

        return new Promise(function(resolve,reject) {
            self.model
                .findOne({ "phone": phone})
                .select("token confirmed")
                .exec(function (err, user) {

                    if(err) return reject(new MongodbException(err));

                    if(!user) return reject(AppException.UNKNOWN_USER);

                    if(user["token"].phone.value != token) return reject(AppException.INVALID_OTP);

                    //lets make sure it hasn't expired
                    let createdDate = user['token'].phone.created;
                    let currentDate = new Date();

                    if(Utils.daysBetween(currentDate,createdDate) > 1 )
                        return Promise.reject(AppException.EXPIRED);

                    //Okay now it hasn't expires so lets do house cleaning
                    user['token'].phone.value = null;
                    user['token'].phone.created = null;

                    //make sure the account is confirmed
                    user['confirmed'].phone = true;

                    resolve(user);
                })

        })
    }


    /**
     * Refresh token on user(using email)
     *
     * @param {string} email
     * @returns {Promise<Object>}
     */
    public refreshEmailToken(email: string): Promise<mongoose.Document> {

        const self = this;

        return new Promise(function(resolve,reject) {

            self.model
                .findOne({'email': email})
                .exec(function (err, user) {

                    if(err) return reject(new MongodbException(err));

                    if(!user) return reject(AppException.UNKNOWN_USER);

                    //add token.value field
                    user['token'].email.value = Utils.generateRandomNumber(6);
                    user['token'].email.created = new Date();

                     resolve(user);

                });

        })

    }


    /**
     * Refresh token on user(using phone)
     *
     * @param {string} phone
     * @returns {Promise<Object>}
     */
    public refreshPhoneToken(phone: string): Promise<mongoose.Document> {

        const self = this;

        return new Promise(function(resolve,reject) {

            self.model
                .findOne({'phone': phone})
                .exec(function (err, user: mongoose.Document) {

                    if(err) return reject(new MongodbException(err));

                    if(!user) return reject(AppException.UNKNOWN_USER);

                    //add token.value field
                    user['token'].phone.value = Utils.generateRandomNumber(6);
                    user['token'].phone.created = new Date();

                     resolve(user);

                });

        })

    }

    public findUserByCredential(email: string, password: string){

        return new Promise((resolve,reject) => {

            this.model
                .findOne({ email: email })
                .select("email name _id password salt confirmed balance type photo roles functions")
                .exec(function(err, doc: mongoose.Document) {
                    if (err)
                        return reject(new MongodbException(err));

                    if (!doc)
                        return reject(AppException.UNKNOWN_USER);

                    if (!doc['authenticate'](password))
                        return reject(AppException.INVALID_PASSWORD);

                    if(!doc['confirmed']["email"] && !doc['confirmed']["phone"])
                        return reject(AppException.ACCOUNT_UNCONFIRMED);

                    return resolve(doc.toJSON());
                })

        });
    }


    public addRolesFunctions(id: string, roles: Array<any>, functions: Array<any>) {
        return this.update({_id: id}, { $addToSet : { functions : {$each : functions}, roles : {$each : roles}}});
    }
}