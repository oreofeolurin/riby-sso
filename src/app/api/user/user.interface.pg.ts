import * as mongoose from "mongoose";

export interface User extends UserWithoutPass{
    password: string
}

export interface UserWithoutPass {
    userId?: number,
    email?: string,
    firstName: string,
    lastName: string,
    phone: string,
    confirmed?: object,
    token?: object,
    roles: Array<mongoose.Types.ObjectId>;
    functions?: Array<string>
}

