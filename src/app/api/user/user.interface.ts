import * as mongoose from "mongoose";

export interface User extends UserWithoutPass{
    password: string
}

export interface UserWithoutPass {
    _id?: string,
    email?: string,
    name: {full : string},
    phone: string,
    roles: Array<mongoose.Types.ObjectId>;
    functions?: Array<string>
}

