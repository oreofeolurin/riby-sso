/**
 * Created by EdgeTech on 12/15/2016.
 */
import * as crypto from 'crypto';
import * as mongoose from 'mongoose';
import {Component} from "@nestjs/common";
import {MongodbHandle} from "../../core/connections/mongodb-handle";
import {RolesFunctions} from "../role/role";
import {Utils} from "../../core/helpers/utils";

@Component()
export class UserModel {
    private model: mongoose.Model<mongoose.Document>;

    constructor(private readonly mongodb: MongodbHandle){
        this.registerModel();
    }


    /**
     * Gets the mongoose Document
     *
     * @return {mongoose.Model<mongoose.Document>}
     */
    public getModel(): mongoose.Model<mongoose.Document>{
        return this.model;
    }

    /**
     * Creates the mongoose model
     */
    private registerModel(){
        //create the Model
        let model = UserModel.createModel(this.mongodb.getHandle());
        this.setModel(model);
    }

    /**
     * Set the mongoose model
     */
    public setModel(model: mongoose.Model<mongoose.Document>){
        this.model = model;
    }


    /**
     * Create the mongoose model using the DBConnection
     *
     * @param DBConnection
     * @return {mongoose.Model<mongoose.Document>}
     */
    public static createModel(DBConnection : mongoose.Connection) : mongoose.Model<mongoose.Document> {

        //create schema
        let schema = this.createSchema();

        //define schema modifier functions
        this.defineSchemaModifiers(schema);

        //define schema methods functions
        this.defineSchemaMethods(schema);

        //define schema static functions
        this.defineSchemaStatics(schema);

        return DBConnection.model('User',schema);


    }

    private static createSchema(): mongoose.Schema {

        let schema = new mongoose.Schema({
            name: {
                full: {
                    type: String,
                    required: "FullName is Required",
                }
            },
            email: {
                type: String,
                index:true,
                unique: true,
                sparse: true,
                match: [/.+\@.+\..+/, "Please fill a valid e-mail address"],
            },
            phone : {
                index:true,
                unique: true,
                type: String,
                required: "Phone Number is Required"
            },

            password: {
                type : String,
                validate: [
                    function (password: string) {
                        return password && password.length > 6;
                    }, 'Password should be longer']
            },
            salt: {
                type: String,
                unique: true,
                sparse: true
            },
            confirmed: {
                email: {
                    type: Boolean,
                    'default': false
                },
                phone :{
                    type: Boolean,
                    'default' : false
                }
            },

            token:{
                email : {
                    value: String,
                    created : Date
                },
                phone : {
                    value: String,
                    created : Date
                }
            },

            roles: [{
                type: mongoose.Schema.Types.ObjectId,
                ref: 'Role'
            }],
            functions: [{
                type: String,
                enum : Utils.enumToArray(RolesFunctions)
            }]

        },{'timestamps':true});

        schema.set('toJSON', { getters: true, virtuals: true, transform: (doc, ret) =>{
            delete ret.id;
            delete ret.password;
            delete ret.confirmed;
            delete ret.salt;
            // delete ret.token;
            return ret;
        }});

        return schema;

    }

    private static defineSchemaModifiers(schema : mongoose.Schema) {

        //Define virtual firstName
        schema.virtual("name.first").get(function (){
            if(this.name.full) return this.name.full.split(' ')[0] || '';
        });

        //Define virtual lastName
        schema.virtual("name.last").get(function (){
            if(this.name.full) return this.name.full.split(' ')[1] ||  '';
        });


    }

    private static defineSchemaMethods(schema : mongoose.Schema) {

        //Define hashPassword method
        schema.method("hashPassword",function (password) {
            return crypto.pbkdf2Sync(password, this.salt, 10000, 64, "sha512").toString('base64');
        });

        //Define authenticate method
        schema.method("authenticate",function (password) {
            return this.password === this.hashPassword(password);
        });

        schema.method("setHashedPassword",function(password){
            //hash the password
            this.salt = new Buffer(crypto.randomBytes(16).toString('base64'), 'base64');
            this.password = this.hashPassword(password);
        })

    }

    private static defineSchemaStatics(schema  : mongoose.Schema) {

        // let isUniqueEmail = await this.operatorModel['ensureEmailUnique'](operatorDetails.email);

        //Define findUniqueAppID
        schema.static("ensureEmailUnique",function(emailToEnsure){
            let self = this;

            return new Promise<boolean>(function(resolve,reject){

                self.findOne({ email : emailToEnsure }, function (err, user) {
                    if(err) return reject(err);

                    if(!user) return resolve(true);

                    else return reject(new Error("RError.EMAIL_EXISTS"));
                });

            })

        })


    }

}