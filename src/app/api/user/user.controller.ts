import {Controller, HttpStatus, UseGuards, Get, Req, Put, Body, Param, Post,} from "@nestjs/common";
import {CreateUserWithRolesAndFunctionsDto, UpdateUserDto, UserIdDto} from "./user.dto";
import {UserService} from "./user.service";
import {RoleFunctions} from "../role/roles.guard";
import {RolesFunctions} from "../role/role";
import {RolesFunctionsDto} from "../role/role.dto";

@Controller('/user')
@UseGuards(/*ApiGuard, RolesGuard*/)
export class UserController{

    constructor(private readonly userService: UserService){}

    @Post("/")
    @RoleFunctions(RolesFunctions.UPDATE_USER_ROLE_AND_FUNCTIONS)
    public async createUserWithRolesAndFunctions(@Body() body: CreateUserWithRolesAndFunctionsDto) {
        await this.userService.createUserWithRolesAndFunctions(body);

        return {code: HttpStatus.OK, message: "Request Successful"};
    }

    @Put("/")
    public async update(@Req() req, @Body() body: UpdateUserDto ) {
        let user = req.locals.user;

        await this.userService.update(user["_id"], body);

        return {code: HttpStatus.OK, message : "Request Successful"};
    }

    @Put("/:userId/role")
    @RoleFunctions(RolesFunctions.UPDATE_USER_ROLE_AND_FUNCTIONS)
    public async addRoleFunctions(@Param() params : UserIdDto, @Body() body: RolesFunctionsDto) {

        await this.userService.addRoleFunctions(params.userId, body);

        return {code: HttpStatus.OK, message : "Request Successful"};
    }

}