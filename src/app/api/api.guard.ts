import {Guard, CanActivate, HttpStatus} from "@nestjs/common";
import {ExecutionContext} from "@nestjs/common/interfaces/execution-context.interface";
import {AuthUtils} from "../core/helpers/auth.utils";
import {Authentications} from "../core/helpers/enums";
import {HttpException} from "@nestjs/core";
import {UserRepository} from "./user/user.repository";
import {RoleService} from "./role/role.service";
import {filter} from "rxjs/operator/filter";
import {Utils} from "../core/helpers/utils";

@Guard()
export class ApiGuard implements CanActivate {

    constructor(private repository: UserRepository,
                private roleService: RoleService) {}

    async canActivate(req, context: ExecutionContext) {

        //lets first check if authorization is in headers or query
        let haystack = Object.assign(req.headers, req.query);

        if (typeof haystack["authorization"] == "undefined")
            throw new HttpException("Unauthorized", HttpStatus.UNAUTHORIZED);

        //now lets verify
        let verified = await this.verifyAuthorization(req, haystack["authorization"]);

        if(!verified) throw new HttpException("Unauthorized", HttpStatus.UNAUTHORIZED);

        return true;
    }


    private async verifyAuthorization(req: Request, authString: string) {

        try {
            let authContent = authString.split(" ");
            let authType = authContent[0];
            let auth = authContent[1];

            //Ensure the right authorisation type is passed
            if (typeof Authentications[authType] == "undefined")
                return false;

            switch (authType) {
                case Authentications.RJT:
                    return this.authenticateRJT(req, auth);

                case Authentications.RSK:
                    return this.authenticateRSK(req, auth);
            }


        } catch (err) {return false}

    }


    /**
     * Authenticate via Riby Json-web Tokens
     * @param req
     * @param token
     * @returns {Promise<any>}
     */
    public async authenticateRJT(req, token){

        let {user} =  await AuthUtils.decodeJwtToken(token);

        // get the valid user document from server
        const userDocument = await this.repository.findOneById(user['_id']);

        // set the user on req.locals
        req.locals = {user : Utils.selectKeys(userDocument, '_id roles functions')};

        return true;
    }



    /**
     * Authenticate via Riby Sentinel Key
     * This provides a user with SYSTEM_ADMIN role
     *
     * @param req
     * @param sentinelKey
     * @returns {Promise<any>}
     */
    public async authenticateRSK(req, sentinelKey){

        if(process.env.SENTINEL_KEY  && (sentinelKey === process.env.SENTINEL_KEY)) {

            //get the SYSTEM_ADMIN_ROLE
            const role = await this.roleService.getFromCacheByName("SYSTEM_ADMIN");

            // set the user on req.locals
            req.locals = {user: {roles: [role._id]}};

            return true;
        }

        return false;

    }

}