import * as bodyParser from 'body-parser';
import * as expressPinoLogger from "express-pino-logger";
import * as cors from "cors";
import {NestFactory} from '@nestjs/core';
import {ApplicationModule} from './app/app.module';
import {INestApplication} from "@nestjs/common";
import {HttpExceptionFilter} from "./app/core/exceptions/http-exception.filter";
import {ParamValidationPipe} from "./app/core/param-validation.pipe";
import {CustomExceptionFilter} from "./app/core/exceptions/custom-exception.filter";

async function bootstrap(port: number) {
    const app: INestApplication = await NestFactory.create(ApplicationModule);
    app.use(bodyParser.json());
    app.use(cors());
    app.use(expressPinoLogger());
    app.setGlobalPrefix("/v1");
    app.useGlobalFilters(new CustomExceptionFilter(), new HttpExceptionFilter());
    app.useGlobalPipes(new ParamValidationPipe());

    await app.listen(port);
    console.log(`Application is listening on port ${port}`);
}

bootstrap(parseInt(process.env.PORT));